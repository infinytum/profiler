package profiler

import (
	"time"
	"log"
)

var (
	callCounter = make(map[string]int)
)

// Measure execution time
func Duration(invocation time.Time, name string) {
	elapsed := time.Since(invocation)
	log.Printf("%s lasted %s", name, elapsed)
}

// Log one call to a function
func Count(name string) {
	if val, exists := callCounter[name]; exists {
		callCounter[name] = val + 1
	} else {
		callCounter[name] = 1
	}
}

// Get the amount of times a function was called
func GetCount(name string) (int) {
	if val, exists := callCounter[name]; exists {
		return val
	}
	return 0
}